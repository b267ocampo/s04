<?php

class Building{
	
	protected $name;
	protected $floors;
	protected $address;


	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
	public function getName(){
		return $this->name;
	}
	public function getFloor(){
		return $this->floors;
	}
	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		return $this->name = $name;
	}

}


$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building{

	public function setName($name){
		return $this->name = $name;
	}

	public function getName(){
		return $this->name;
	}

	public function setFloor($floors){
		return $this->floors = $floors;
	}



	public function setAddress($address){
		return $this->address = $address;
	}
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');



